package com.ema.mypasswords.di

import android.content.SharedPreferences
import com.ema.mypasswords.data.db.PasswordsDao
import com.ema.mypasswords.data.repo.main.MainRepo
import com.ema.mypasswords.data.repo.main.MainRepoImpl
import com.ema.mypasswords.data.repo.password.PasswordRepo
import com.ema.mypasswords.data.repo.password.PasswordRepoImpl
import com.ema.mypasswords.data.repo.register.RegisterRepo
import com.ema.mypasswords.data.repo.register.RegisterRepoImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryProvider {

    @Provides
    @Singleton
    fun providePasswordRepo(dao: PasswordsDao): PasswordRepo = PasswordRepoImpl(dao)

    @Provides
    @Singleton
    fun provideMainRepo(dao: PasswordsDao): MainRepo = MainRepoImpl(dao)

    @Provides
    @Singleton
    fun provideRegisterRepo(sharedPreferences: SharedPreferences): RegisterRepo =
        RegisterRepoImpl(sharedPreferences)


}