package com.ema.mypasswords.di

import android.content.Context
import com.ema.mypasswords.utils.Constants.MY_SHARED_PREFERENCES
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object SharedPrefrencesProvider {

    @Provides
    @Singleton
    fun provideSharedPrefrences(@ApplicationContext context: Context) =
        context.getSharedPreferences(MY_SHARED_PREFERENCES, Context.MODE_PRIVATE)
}

