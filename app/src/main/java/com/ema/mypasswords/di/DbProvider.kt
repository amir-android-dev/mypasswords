package com.ema.mypasswords.di

import android.content.Context
import androidx.room.Room
import com.ema.mypasswords.data.db.PasswordsDatabase
import com.ema.mypasswords.data.model.PasswordsEntity
import com.ema.mypasswords.utils.Constants.PASSWORDS_DATABASE
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DbProvider {

    @Provides
    @Singleton
    fun entity() = PasswordsEntity()

    @Provides
    @Singleton
    fun dao(database: PasswordsDatabase) = database.dao()

    @Provides
    @Singleton
    fun database(@ApplicationContext context: Context) =
        Room.databaseBuilder(context, PasswordsDatabase::class.java, PASSWORDS_DATABASE)
            .fallbackToDestructiveMigration()
            .allowMainThreadQueries()
            .build()
}