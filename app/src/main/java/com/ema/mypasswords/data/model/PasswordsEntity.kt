package com.ema.mypasswords.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.ema.mypasswords.utils.Constants.PASSWORD_TABLE

@Entity(tableName = PASSWORD_TABLE)
data class PasswordsEntity(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    var title: String = "",
    var password: String = "",
    var category: String = "",
    var priority: String = ""
)