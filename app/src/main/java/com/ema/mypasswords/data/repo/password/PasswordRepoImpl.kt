package com.ema.mypasswords.data.repo.password

import com.ema.mypasswords.data.db.PasswordsDao
import com.ema.mypasswords.data.model.PasswordsEntity
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class PasswordRepoImpl @Inject constructor(private val dao: PasswordsDao) : PasswordRepo {
    override suspend fun insertPassword(password: PasswordsEntity) = dao.insertPassword(password)
    override fun getPassword(id: Int): Flow<PasswordsEntity> = dao.getPassword(id)
}