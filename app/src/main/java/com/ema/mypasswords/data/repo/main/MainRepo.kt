package com.ema.mypasswords.data.repo.main



import com.ema.mypasswords.data.model.PasswordsEntity
import kotlinx.coroutines.flow.Flow

interface MainRepo {

    suspend fun deletePassword(password: PasswordsEntity)

    fun getAllPasswords(): Flow<List<PasswordsEntity>>

    suspend fun updatePassword(password: PasswordsEntity)
    fun filterPasswords(priority: String): Flow<List<PasswordsEntity>>

    fun searchPassword(title: String): Flow<List<PasswordsEntity>>
}