package com.ema.mypasswords.data.repo.register

import android.content.SharedPreferences
import com.ema.mypasswords.utils.Constants.SHARED_PREFERENCES_PASS
import com.ema.mypasswords.utils.Constants.SHARED_PREFERENCES_USERNAME
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import javax.inject.Inject

class RegisterRepoImpl @Inject constructor(private val sharedPreferences: SharedPreferences) :
    RegisterRepo {

    override fun saveRegisterData(username: String, password: String) {
        val editor = sharedPreferences.edit()
        editor.putString(SHARED_PREFERENCES_USERNAME, username)
        editor.putString(SHARED_PREFERENCES_PASS, password)
        editor.apply()
    }

    override fun readRegisterData(): Flow<Pair<String, String>> {
        val username = sharedPreferences.getString(SHARED_PREFERENCES_USERNAME, null)
        val password = sharedPreferences.getString(SHARED_PREFERENCES_PASS, null)
        return flowOf(Pair(username!!, password!!))
    }


}