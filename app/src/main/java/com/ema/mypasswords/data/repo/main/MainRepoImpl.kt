package com.ema.mypasswords.data.repo.main

import com.ema.mypasswords.data.db.PasswordsDao
import com.ema.mypasswords.data.model.PasswordsEntity
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class MainRepoImpl @Inject constructor(private val dao: PasswordsDao) : MainRepo {

    override suspend fun deletePassword(password: PasswordsEntity) = dao.deletePassword(password)

    override fun getAllPasswords(): Flow<List<PasswordsEntity>> = dao.getAllPasswords()
    override suspend fun updatePassword(password: PasswordsEntity) = dao.updatePassword(password)

    override fun filterPasswords(priority: String): Flow<List<PasswordsEntity>> =
        dao.filterPasswords(priority)

    override fun searchPassword(title: String): Flow<List<PasswordsEntity>> =
        dao.searchPassword(title)

}