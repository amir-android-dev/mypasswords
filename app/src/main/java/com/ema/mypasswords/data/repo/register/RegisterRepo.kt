package com.ema.mypasswords.data.repo.register

import kotlinx.coroutines.flow.Flow

interface RegisterRepo {

    fun saveRegisterData(username: String, password: String)

    fun readRegisterData(): Flow<Pair<String?, String?>>
}