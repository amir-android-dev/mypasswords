package com.ema.mypasswords.data.repo.password


import com.ema.mypasswords.data.model.PasswordsEntity
import kotlinx.coroutines.flow.Flow

interface PasswordRepo {
    suspend fun insertPassword(password: PasswordsEntity)
    fun getPassword(id: Int): Flow<PasswordsEntity>
}