package com.ema.mypasswords.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.ema.mypasswords.data.model.PasswordsEntity

@Database(
    entities = [PasswordsEntity::class],
    version = 1,
    exportSchema = true
)
abstract class PasswordsDatabase : RoomDatabase() {

    abstract fun dao(): PasswordsDao
}