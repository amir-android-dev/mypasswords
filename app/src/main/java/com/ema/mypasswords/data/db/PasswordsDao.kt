package com.ema.mypasswords.data.db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.ema.mypasswords.data.model.PasswordsEntity
import com.ema.mypasswords.utils.Constants.PASSWORD_TABLE
import kotlinx.coroutines.flow.Flow

@Dao
interface PasswordsDao {

    @Insert
    suspend fun insertPassword(password: PasswordsEntity)

    @Delete
    suspend fun deletePassword(password: PasswordsEntity)

    @Update
    suspend fun updatePassword(password: PasswordsEntity)

    @Query("select * from $PASSWORD_TABLE")
    fun getAllPasswords(): Flow<List<PasswordsEntity>>

    @Query("select * from $PASSWORD_TABLE where id == :id")
    fun getPassword(id: Int): Flow<PasswordsEntity>

    @Query("select * from $PASSWORD_TABLE where priority == :priority")
    fun filterPasswords(priority: String): Flow<List<PasswordsEntity>>

    @Query("select * from $PASSWORD_TABLE where title like '%' || :title || '%' ")
    fun searchPassword(title: String): Flow<List<PasswordsEntity>>

}