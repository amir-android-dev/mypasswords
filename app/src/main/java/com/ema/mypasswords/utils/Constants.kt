package com.ema.mypasswords.utils

import com.ema.mypasswords.data.db.PasswordsDatabase

object Constants {
    //db
    const val PASSWORDS_DATABASE = "passwords_database"
    const val PASSWORD_TABLE = "password_table"

    //shared pref
    const val MY_SHARED_PREFERENCES = "my_shared_preferences"
    const val SHARED_PREFERENCES_USERNAME = "shared_preferences_username"
    const val SHARED_PREFERENCES_PASS = "shared_preferences_pass"

    //category
    const val WORK = "WORK"
    const val EDUCATION = "EDUCATION"
    const val BANK = "BANK"
    const val EMAIL = "EMAIL"
    const val ELSE = "ELSE"
    //priority
    const val HIGH = "HIGH"
    const val NORMAL = "NORMAL"
    const val LOW = "LOW"

    //popup menu items
    const val DELETE = "delete"
    const val EDIT = "edit"
    const val NEW = "new"
    const val BUNDLE_ID = "bundle_id"



}