package com.ema.mypasswords.ui.register

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.lifecycle.lifecycleScope
import com.ema.mypasswords.databinding.ActivityRegisterBinding
import com.ema.mypasswords.ui.main.MainActivity
import com.ema.mypasswords.utils.displayToast
import com.ema.mypasswords.view.register.RegisterIntent
import com.ema.mypasswords.view.register.RegisterState
import com.ema.mypasswords.view.register.RegisterViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class RegisterActivity : AppCompatActivity() {
    //binding
    private var _binding: ActivityRegisterBinding? = null
    private val binding get() = _binding!!

    //other
    private val viewModel: RegisterViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        lifecycleScope.launch {
            viewModel.registerIntent.send(RegisterIntent.GetRegisterData)
        }

        lifecycleScope.launch {
            viewModel.state.collect { state ->
                when (state) {
                    is RegisterState.ErrorResponse -> {
                        displayToast(this@RegisterActivity, state.msg)
                        Log.e("regis", state.msg)
                    }

                    is RegisterState.GetRegisterData -> {
                        binding.btnRegister.text = state.btn
                        binding.etUsername.setText(state.username)
                    }

                    is RegisterState.SaveRegisterData -> {
                        binding.btnRegister.text = state.btn
                    }

                    RegisterState.Login -> {
                        val intent = Intent(this@RegisterActivity, MainActivity::class.java)
                        startActivity(intent)
                        binding.etPassword.setText("")
                    }
                    RegisterState.Idle -> {}
                }
            }
        }

        binding.btnRegister.setOnClickListener {
            lifecycleScope.launch {
                val username = binding.etUsername.text.toString()
                val pass = binding.etPassword.text.toString()
                when (binding.btnRegister.text.toString()) {
                    "LOGIN" -> {
                        viewModel.registerIntent.send(RegisterIntent.Login(username, pass))
                    }

                    "Register" -> {
                        lifecycleScope.launch {
                            viewModel.registerIntent.send(
                                RegisterIntent.SaveRegisterData(
                                    username,
                                    pass
                                )
                            )
                        }
                    }
                }
            }
        }

        //get saved pass
        lifecycleScope.launch {
            viewModel.registerIntent.send(RegisterIntent.GetRegisterData)
            Log.e("regis", "state")
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}