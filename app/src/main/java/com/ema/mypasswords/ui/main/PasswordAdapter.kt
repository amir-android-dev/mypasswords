package com.ema.mypasswords.ui.main

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.PopupMenu
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.ema.mypasswords.R
import com.ema.mypasswords.data.model.PasswordsEntity
import com.ema.mypasswords.databinding.ItemPasswordBinding
import com.ema.mypasswords.utils.Constants.DELETE
import com.ema.mypasswords.utils.Constants.EDIT
import com.ema.mypasswords.utils.Constants.HIGH
import com.ema.mypasswords.utils.Constants.LOW
import com.ema.mypasswords.utils.Constants.NORMAL
import javax.inject.Inject

class PasswordAdapter @Inject constructor() :
    ListAdapter<PasswordsEntity, PasswordAdapter.PassViewHolder>(object :
        DiffUtil.ItemCallback<PasswordsEntity>() {
        override fun areItemsTheSame(oldItem: PasswordsEntity, newItem: PasswordsEntity) =
            oldItem.id == newItem.id

        override fun areContentsTheSame(
            oldItem: PasswordsEntity,
            newItem: PasswordsEntity
        ) = oldItem == newItem
    }) {

    private lateinit var binding: ItemPasswordBinding
    private lateinit var context: Context
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PassViewHolder {
        context = parent.context
        binding = ItemPasswordBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PassViewHolder()
    }

    override fun onBindViewHolder(holder: PassViewHolder, position: Int) {
        holder.bind(currentList[position])
    }

    inner class PassViewHolder : ViewHolder(binding.root) {
        fun bind(item: PasswordsEntity) {
            binding.apply {
                tvPassword.text = item.password
                tvTitle.text = item.title
                //category
                when (item.priority) {
                    HIGH -> binding.priorityColor.setBackgroundColor(ContextCompat.getColor(context,R.color.red))
                    NORMAL -> binding.priorityColor.setBackgroundColor(ContextCompat.getColor(context,R.color.green))
                    LOW -> binding.priorityColor.setBackgroundColor(ContextCompat.getColor(context,R.color.yellow))
                }

                //menu
                ivMenu.setOnClickListener {
                    val popupMenu = PopupMenu(context, it)
                    popupMenu.menuInflater.inflate(R.menu.menu_items, popupMenu.menu)
                    popupMenu.show()
                    //select
                    popupMenu.setOnMenuItemClickListener { menuItem ->
                        when (menuItem.itemId) {
                            R.id.itemDelete -> {
                                onItemClickListener?.let {
                                    it(item, DELETE)
                                }
                            }

                            R.id.itemEdit -> {
                                onItemClickListener?.let {
                                    it(item, EDIT)
                                }
                            }

                        }
                        return@setOnMenuItemClickListener true
                    }
                }
            }
        }
    }

    private var onItemClickListener: ((PasswordsEntity, String) -> Unit)? = null
    fun setonItemClickListener(listener: (PasswordsEntity, String) -> Unit) {
        onItemClickListener = listener
    }

}