package com.ema.mypasswords.ui.password

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.ema.mypasswords.data.model.PasswordsEntity
import com.ema.mypasswords.databinding.FragmentPasswordBinding
import com.ema.mypasswords.utils.setupListWithAdapter
import com.ema.mypasswords.view.password.PasswordIntent
import com.ema.mypasswords.view.password.PasswordState
import com.ema.mypasswords.view.password.PasswordViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class PasswordFragment : BottomSheetDialogFragment() {
    //binding
    private var _binding: FragmentPasswordBinding? = null
    private val binding get() = _binding!!

    //other
    private val viewModel: PasswordViewModel by viewModels()

    @Inject
    lateinit var entity: PasswordsEntity
    private var category = ""
    private var priority = ""
    private var passId = 0
    private val categoriesList: MutableList<String> = mutableListOf()
    private val prioritiesList: MutableList<String> = mutableListOf()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentPasswordBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //InitViews
        binding.apply {
            //Close
            ivClose.setOnClickListener { dismiss() }
            //Data
            lifecycleScope.launch {
                //Send
                viewModel.passwordIntent.send(PasswordIntent.SpinnerList)

                //Get data
                viewModel.state.collect { state ->
                    when (state) {
                        is PasswordState.Idle -> {}
                        is PasswordState.SpinnersData -> {
                            //Category
                            categoriesList.addAll(state.categoriesList)
                            spinnerCategory.setupListWithAdapter(state.categoriesList.toMutableList()) {
                                category = it
                            }
                            //Priority
                            prioritiesList.addAll(state.prioritiesList)
                            spinnerPriority.setupListWithAdapter(state.prioritiesList.toMutableList()) {
                                priority = it
                            }
                        }
                        is PasswordState.SavePassword -> {
                            dismiss()
                        }
                        is PasswordState.Error -> {
                            Toast.makeText(requireContext(), state.msg, Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }
            //Save
            btnSavePass.setOnClickListener {
                val title = etTitle.text.toString()
                val pass = etPassword.text.toString()
                entity.id = passId
                entity.title = title
                entity.password = pass
                entity.category = category
                entity.priority = priority

                lifecycleScope.launch {
                        viewModel.passwordIntent.send(PasswordIntent.SavePassword(entity))

                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }


}