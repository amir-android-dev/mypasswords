package com.ema.mypasswords.ui.splash

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import com.ema.mypasswords.R
import com.ema.mypasswords.databinding.ActivitySplashBinding
import com.ema.mypasswords.ui.register.RegisterActivity
import dagger.hilt.android.AndroidEntryPoint
import io.github.inflationx.calligraphy3.BuildConfig
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@AndroidEntryPoint
@SuppressLint("SetTextI18n")
class SplashActivity : AppCompatActivity() {
    private var _binding: ActivitySplashBinding? = null
    private val binding get() = _binding!!
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)

        lifecycleScope.launch {
            delay(2000)
            val intent = Intent(this@SplashActivity, RegisterActivity::class.java)
            startActivity(intent)
        }
        binding.tvVersion.text = "${getString(R.string.version)}: ${BuildConfig.VERSION_NAME}"

    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}