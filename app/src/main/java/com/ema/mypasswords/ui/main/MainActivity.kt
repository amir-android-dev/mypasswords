package com.ema.mypasswords.ui.main

import android.app.Dialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.ViewGroup
import android.view.Window
import androidx.activity.viewModels
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.ema.mypasswords.R
import com.ema.mypasswords.data.model.PasswordsEntity
import com.ema.mypasswords.databinding.ActivityMainBinding
import com.ema.mypasswords.databinding.DialogUpdatePasswordBinding
import com.ema.mypasswords.ui.password.PasswordFragment
import com.ema.mypasswords.utils.Constants.DELETE
import com.ema.mypasswords.utils.Constants.EDIT
import com.ema.mypasswords.utils.displayToast
import com.ema.mypasswords.utils.getIndexFromList
import com.ema.mypasswords.utils.setupListWithAdapter
import com.ema.mypasswords.utils.setupRecycler
import com.ema.mypasswords.view.main.MainIntent
import com.ema.mypasswords.view.main.MainState
import com.ema.mypasswords.view.main.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private var _binding: ActivityMainBinding? = null
    private val binding get() = _binding!!

    //other
    private val viewModel: MainViewModel by viewModels()
    @Inject lateinit var passwordsEntity: PasswordsEntity

    @Inject
    lateinit var adapter: PasswordAdapter
    private val categoriesList: MutableList<String> = mutableListOf()
    private val prioritiesList: MutableList<String> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        //toolbar
        setSupportActionBar(binding.toolbar)
        //add
        binding.flAddPass.setOnClickListener {
            PasswordFragment().show(supportFragmentManager, PasswordFragment().tag)
        }
        //action
        viewModel.handleIntent(MainIntent.LoadAllPasswords)
        viewModel.handleIntent(MainIntent.SpinnerList)

        //state
        lifecycleScope.launch {
            viewModel.state.collect { state ->
                when (state) {
                    MainState.EmptyList -> {
                        binding.emptyLay.isVisible = true
                        binding.rvPass.isVisible = false
                    }

                    is MainState.LoadPasswords -> {
                        binding.emptyLay.isVisible = false
                        binding.rvPass.isVisible = true
                        binding.rvPass.setupRecycler(
                            adapter,
                            LinearLayoutManager(
                                this@MainActivity,
                                LinearLayoutManager.VERTICAL,
                                false
                            )
                        )
                        adapter.submitList(state.passwords)
                        adapter.setonItemClickListener { entity, type ->
                            when (type) {
                                EDIT -> {
                                    setupUpdateDialog(entity)
                                }

                                DELETE -> {
                                    viewModel.handleIntent(MainIntent.DeletePassword(entity))

                                }
                            }
                        }
                    }
                    is MainState.DeletePass -> {
                        displayToast(this@MainActivity, "Successfully deleted")
                    }

                    is MainState.SpinnerData -> {
                        prioritiesList.addAll(state.priorities)
                        categoriesList.addAll(state.categories)
                    }

                    is MainState.DisplayError -> {
                        displayToast(this@MainActivity, state.msg)
                    }

                    is MainState.UpdatePass -> {

                        displayToast(this@MainActivity, "Successfully Updated")
                    }
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_toolbar, menu)
        val search = menu!!.findItem(R.id.actionSearch)
        val searchView = search.actionView as SearchView
        searchView.queryHint = getString(R.string.search_hint)
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                viewModel.handleIntent(MainIntent.SearchPassword(newText))
                return true
            }
        })
        return super.onCreateOptionsMenu(menu)
    }

    private fun setupUpdateDialog(entity: PasswordsEntity) {
        //binding
        val bindingDialog = DialogUpdatePasswordBinding.inflate(layoutInflater)
        val dialog = Dialog(this)
        dialog.setContentView(bindingDialog.root)
        //set data
        bindingDialog.etTitle.setText(entity.title)
        bindingDialog.etPassword.setText(entity.password)
        //spinner setup
        bindingDialog.spinnerPriority.setupListWithAdapter(prioritiesList) { entity.category = it}
        bindingDialog.spinnerCategory.setupListWithAdapter(categoriesList) { entity.priority = it}
        bindingDialog.spinnerCategory.setSelection(categoriesList.getIndexFromList(entity.category))
        bindingDialog.spinnerPriority.setSelection(prioritiesList.getIndexFromList(entity.priority))
        //cancel
        bindingDialog.btnCancel.setOnClickListener {
            dialog.dismiss()
        }
        //update
        bindingDialog.btnUpate.setOnClickListener {
            val updatedEntity = entity.copy(
                title = bindingDialog.etTitle.text.toString(),
                password = bindingDialog.etPassword.text.toString(),
                category = entity.category,
                priority = entity.priority
            )
            viewModel.handleIntent(MainIntent.UpdatePassword(updatedEntity))
            dialog.dismiss()
        }
        matchDialogToUi(dialog)
        dialog.create()
        dialog.show()
    }

    //to match dialog to UI
    private fun matchDialogToUi(dialog: Dialog) {
        val window: Window? = dialog.window
        window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}