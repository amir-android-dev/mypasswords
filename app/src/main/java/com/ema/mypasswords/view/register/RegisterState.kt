package com.ema.mypasswords.view.register

sealed class RegisterState {

    data class SaveRegisterData(val unit: Unit,val btn:String) : RegisterState()
    data class GetRegisterData(val username: String, val password: String,val btn: String) :
        RegisterState()
    object Login : RegisterState()
    object Idle : RegisterState()
    data class ErrorResponse(val msg: String) : RegisterState()
}