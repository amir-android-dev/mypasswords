package com.ema.mypasswords.view.main

import com.ema.mypasswords.data.model.PasswordsEntity

sealed class MainIntent {
    object LoadAllPasswords : MainIntent()
    data class SearchPassword(val search: String) : MainIntent()
    data class DeletePassword(val entity: PasswordsEntity) : MainIntent()
    data class UpdatePassword(val entity: PasswordsEntity) : MainIntent()
    object SpinnerList : MainIntent()

}