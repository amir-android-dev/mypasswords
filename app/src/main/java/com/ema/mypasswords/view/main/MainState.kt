package com.ema.mypasswords.view.main

import com.ema.mypasswords.data.model.PasswordsEntity

sealed class MainState {
    data class LoadPasswords(val passwords: List<PasswordsEntity>) : MainState()
    object EmptyList : MainState()
    data class DeletePass(val unit: Unit) : MainState()
    data class UpdatePass(val unit: Unit) : MainState()

    data class DisplayError(val msg: String) : MainState()
    data class SpinnerData(
        val priorities: MutableList<String>,
        val categories: MutableList<String>
    ) : MainState()

}