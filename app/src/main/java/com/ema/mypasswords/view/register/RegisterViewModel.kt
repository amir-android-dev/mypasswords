package com.ema.mypasswords.view.register

import android.annotation.SuppressLint
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ema.mypasswords.data.repo.register.RegisterRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RegisterViewModel @Inject constructor(val repo: RegisterRepo) : ViewModel() {
    init {
        handleIntent()
    }

    val registerIntent = Channel<RegisterIntent>()

    private val _state = MutableStateFlow<RegisterState>(RegisterState.Idle)
    val state: StateFlow<RegisterState>
        get() = _state

    private fun handleIntent() = viewModelScope.launch {
        registerIntent.consumeAsFlow().collect { intent ->
            when (intent) {
                is RegisterIntent.SaveRegisterData -> saveRegisterData(
                    intent.username,
                    intent.password
                )

                is RegisterIntent.GetRegisterData -> getRegisterData()
                is RegisterIntent.Login -> navigateToMainActivity(intent.username, intent.password)
            }
        }
    }

    private fun navigateToMainActivity(username: String, password: String) = viewModelScope.launch {
        repo.readRegisterData().collect {
            if (it.first == username && it.second == password) {
                _state.value = RegisterState.Login
            } else {
                _state.value = RegisterState.ErrorResponse("Your Username or password is incorrect")
            }
        }

    }

    @SuppressLint("SetTextI18n")
    private fun getRegisterData() = viewModelScope.launch {
        try {
            repo.readRegisterData().collect {
                if (it.second!!.isEmpty()) {
                    _state.value = RegisterState.ErrorResponse("Please Register yourself")
                } else {
                    _state.value = RegisterState.GetRegisterData(it.first!!, it.second!!, "LOGIN")
                }
            }
        } catch (e: Exception) {
            _state.value = RegisterState.ErrorResponse("no password is saved. Pls register!")
        }
    }

    private fun saveRegisterData(username: String, pass: String) {
        if (username.isEmpty() || pass.isEmpty()) {
            _state.value = RegisterState.ErrorResponse("Your password or username is empty")
        } else if (username.length < 5 || pass.length < 5) {
            _state.value =
                RegisterState.ErrorResponse("Your password or username must be longer than 5 characters")
        } else {
            _state.value =
                RegisterState.SaveRegisterData(repo.saveRegisterData(username, pass), "LOGIN")
        }


    }

}