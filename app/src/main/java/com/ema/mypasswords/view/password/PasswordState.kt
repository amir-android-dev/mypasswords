package com.ema.mypasswords.view.password



sealed class PasswordState {
    object Idle : PasswordState()
    data class SpinnersData(val categoriesList: List<String>, val prioritiesList: List<String>) :
        PasswordState()

    data class Error(val msg: String) : PasswordState()
    data class SavePassword(val unit: Unit) : PasswordState()



}
