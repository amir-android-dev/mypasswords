package com.ema.mypasswords.view.password

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ema.mypasswords.data.model.PasswordsEntity
import com.ema.mypasswords.data.repo.password.PasswordRepo
import com.ema.mypasswords.utils.Constants.BANK
import com.ema.mypasswords.utils.Constants.EDUCATION
import com.ema.mypasswords.utils.Constants.ELSE
import com.ema.mypasswords.utils.Constants.EMAIL
import com.ema.mypasswords.utils.Constants.HIGH
import com.ema.mypasswords.utils.Constants.LOW
import com.ema.mypasswords.utils.Constants.NORMAL
import com.ema.mypasswords.utils.Constants.WORK
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PasswordViewModel @Inject constructor(private val repo: PasswordRepo) : ViewModel() {
    init {
        handleIntents()
    }

    val passwordIntent = Channel<PasswordIntent>()

    private val _state = MutableStateFlow<PasswordState>(PasswordState.Idle)
    val state get() = _state

    private fun handleIntents() = viewModelScope.launch {
        passwordIntent.consumeAsFlow().collect { intent ->
            when (intent) {
                is PasswordIntent.SavePassword -> savingPassword(intent.entity)
                is PasswordIntent.SpinnerList -> fetchSpinnerList()
            }
        }
    }


    private fun savingPassword(passwordsEntity: PasswordsEntity) = viewModelScope.launch {
        try {
            _state.value = PasswordState.SavePassword(repo.insertPassword(passwordsEntity))
        } catch (e: Exception) {
            _state.value = PasswordState.Error(e.message.toString())
        }
    }

    private fun fetchSpinnerList() {
        val categoryList = mutableListOf(WORK, EDUCATION, BANK, EMAIL, ELSE)
        val priorityList = mutableListOf(HIGH, NORMAL, LOW)
        _state.value = PasswordState.SpinnersData(categoryList, priorityList)
    }


}