package com.ema.mypasswords.view.password

import com.ema.mypasswords.data.model.PasswordsEntity

sealed class PasswordIntent {
    object SpinnerList : PasswordIntent()
    data class SavePassword(val entity: PasswordsEntity) : PasswordIntent()


}
