package com.ema.mypasswords.view.register


sealed class RegisterIntent {

    data class SaveRegisterData(val username: String, val password: String) : RegisterIntent()

    object GetRegisterData : RegisterIntent()
    data class Login(val username: String, val password: String) : RegisterIntent()
}