package com.ema.mypasswords.view.main

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ema.mypasswords.data.model.PasswordsEntity
import com.ema.mypasswords.data.repo.main.MainRepo
import com.ema.mypasswords.utils.Constants.BANK
import com.ema.mypasswords.utils.Constants.EDUCATION
import com.ema.mypasswords.utils.Constants.ELSE
import com.ema.mypasswords.utils.Constants.EMAIL
import com.ema.mypasswords.utils.Constants.HIGH
import com.ema.mypasswords.utils.Constants.LOW
import com.ema.mypasswords.utils.Constants.NORMAL
import com.ema.mypasswords.utils.Constants.WORK

import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val repo: MainRepo) : ViewModel() {

    private val _state = MutableStateFlow<MainState>(MainState.EmptyList)
    val state get() = _state

    fun handleIntent(intent: MainIntent) = viewModelScope.launch {
        when (intent) {
            MainIntent.LoadAllPasswords -> fetchingAllPasswords()
            is MainIntent.DeletePassword -> deletePassword(intent.entity)
            is MainIntent.SearchPassword -> fetchingSearch(intent.search)
            is MainIntent.SpinnerList -> fetchSpinnerData()
            is MainIntent.UpdatePassword -> updatePassword(intent.entity)
        }
    }

    private suspend fun updatePassword(entity: PasswordsEntity) {
        _state.value = try {
            Log.e("update viewmodel","update")
            MainState.UpdatePass(repo.updatePassword(entity))

        } catch (e: Exception) {
            MainState.DisplayError(e.message.toString())
        }
    }

    private fun fetchSpinnerData() {
        val categoryList = mutableListOf(WORK, EDUCATION, BANK, EMAIL, ELSE)
        val priorityList = mutableListOf(HIGH, NORMAL, LOW)
        _state.value = MainState.SpinnerData(priorityList, categoryList)
    }

    private suspend fun deletePassword(entity: PasswordsEntity) {
        _state.value = MainState.DeletePass(repo.deletePassword(entity))
    }

    private suspend fun fetchingSearch(search: String) {
        repo.searchPassword(search).collect { searchedPasswords ->
            if (searchedPasswords.isEmpty()) {
                _state.value = MainState.EmptyList
            } else {
                _state.value = MainState.LoadPasswords(searchedPasswords)
            }
        }
    }

    private suspend fun fetchingAllPasswords() {
        val data = repo.getAllPasswords()
        data.collect {
            _state.value = if (it.isEmpty()) {
                MainState.EmptyList
            } else {
                MainState.LoadPasswords(it)
            }
        }
    }
}